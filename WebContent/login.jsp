<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
<style>
        body {
            	font: normal 13px/20px Arial, Helvetica, sans-serif;
            	word-wrap: break-word;
            	color: #eee;
            }
		header {
				background-image:url("headerbg.jpg");
				padding: 30px;
				color: white;
			}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login to National Awards</title>
</head>
<body>
	<form action="bean.jsp">
		<header class="w3-serif">
		<table width="100%">
			<tr>
			<td align="left" ><img src="logo.jpg" height="100px" width="120px"></td>
			<td><h1><font face="georgia" size="25px" color="white">National Awards 2018</font></h1></td>
			<td align="right"><div><font face="georgia" size="4" color="white">Come see the winners on <br> 17 September 2018 <br>12 noon</font><div></td>
			</tr>
		</table>
		</header>
	</form>
	
	<br> <br> <br>
	
	<div id="signup" align="center" >
		<form action="loginServlet" method="post">
        	<fieldset style="width: 300px">
            	<legend> <font face="verdana" color="black" size="4"><b>Sign up</b></font></legend>
            	<table>
                <tr> 
                    <td><font face="verdana" color="blue" size="3">User ID </font></td>
                    <td><input type="text" name="username" required="required" /></td>
                </tr>
                <tr>
                    <td><font face="verdana" color="blue" size="3">Password</font></td>
                    <td><input type="password" name="password" required="required" /></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Login" /></td>
                </tr>
            	</table>
        	</fieldset>
    	</form>
	</div> 
	<br><br><br><br><br><br>
	<div id="footer" style="background-color:#C0C0C0" align="center"> <br> <b> Follow us on <b> <br> <br> 
		<a href="#"> <img src="images/fb.png" height="30" width="30"> </img> </a> 
		<a href="#"> <img src="images/Instagram.png" height="30" width="30"> </img>  </a> 
		<a href="#"> <img src="images/twitter.png" height="30" width="30"> </img>  </a> <br> <br> </div> 
		<div style="display:table" align="center">
    			<div style="display: inline-block;width:400px;height:30px;background-color:#C0C0C0;" align="center"><a href="TermsOfUse.jsp"> Term of Use</a> </div>
    			<div style="display: inline-block;width:355px;height:30px;background-color:#C0C0C0;" align="right">Contact Us : </div>
   				<div style="display: inline-block;width:360px;height:30px;background-color:#C0C0C0;" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lttsNationalAwards@gmail.com</div>
    			<div style="display: inline-block;width:400px;height:30px;background-color:#C0C0C0;" align="center"><a href="PrivacyPolicy.jsp">Privacy Policy</a></div>
		</div>
	<div style="background-color:#666666" align="center"> <br><font face="Calibri" > Copyright 2008 by Refrence data , Updated on 20th Sep 2018<br> </div>
	
	  
</body>
</html>

