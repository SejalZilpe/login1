<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
	a {text-decoration: none}
	<!-- style for header -->
	body {
            font-family: normal 13px/20px Arial, Helvetica, sans-serif;
            word-wrap: break-word;
            color: #eee;
            margin: 0;
            }
	header {
			background-image:url("headerbg.jpg");
			padding: 30px;
			color: white;
			}
			
	<!-- style for tab -->
        * {box-sizing: border-box;}
	.topnav {
  				overflow: hidden;
  				background-color: #e9e9e9;
			}
	.topnav a {
  				float: left;
  				display: block;
  				color: black;
  				text-align: center;
  				padding: 14px 16px;
  				text-decoration: none;
  				font-size: 17px;
			}
	.topnav a:hover {
 			 	background-color: #ddd;
  				color: black;
			}
	.topnav a.active {
  				background-color: #8094a3;
  				color: white;
			}
			
	<!-- style for image -->
	.mySlides {display:none;} 
	
	<!-- style for sandbox 	-->
	#div1 {
  				font-size:48px;
			}   
			
	<!-- style for timer -->  
	 #clockdiv {
            	font-family: sans-serif;
            	color: #FFFFFF;
            	display: inline-block;
            	font-weight: 100;
            	text-align: center;
            	font-size: 30px;
        	}
  	#clockdiv > div {
                padding: 10px;
                border-radius: 3px;
                background: #C0C0C0;
                display: inline-block;
            }
 	#clockdiv div > span {
                padding: 15px;
                border-radius: 3px;
                background: #A52A2A;
                display: inline-block;
            }
 	.smalltext {
            padding-top: 5px;
            font-size: 16px;
        	}
        	
        	      
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome to National Awards</title>
</head>
<body>
	<form action="bean.jsp">
		<header class="w3-serif">
		<table width="100%">
			<tr>
			<td align="left" ><img src="logo.jpg" height="100px" width="120px"></td>
			<td><h1><font face="georgia" size="25px" color="white">National Awards 2018</font></h1></td>
			<td align="right"><div><font face="georgia" size="4" color="white">Come see the winners on <br> 17 September 2018 <br>12 noon</font><div></td>
			</tr>
		</table>
		</header>
	</form>
		<br>
	<div id="tabs" align="center" class="topnav" >
  		<a class="active" href="#home">Home</a>
  		<a href="#about">Nomination</a>
  		<a href="login.jsp">Critic's Login</a>
	</div>

 	<div id="imagerslider" class="w3-content w3-display-container">
  		<img class="mySlides" src="images/hdgold3.jpg" style="width:100%">
  		<img class="mySlides" src="images/hdraazi.jpg" style="width:100%">
  		<img class="mySlides" src="images/hdpadmavat.jpg" style="width:100%">
  		<img class="mySlides" src="images/hdgold2.jpg" style="width:100%">
  		<img class="mySlides" src="images/hdsanju2.jpg" style="width:100%">
  		<img class="mySlides" src="images/hdsonu.jpg" style="width:100%">
  		<img class="mySlides" src="images/hdgold.jpg" style="width:100%">
  		<img class="mySlides" src="images/hdsanju1.jpg" style="width:100%">

  		<button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
  		<button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
	</div>
  
  	<br><br><br>
  
  	<div id="quote" align="center">
  		<font face="adobe arabic" color="brown" size="6" align="center"><b><i>
  			When I look down at this golden statue, may it remind me and <br>
  			every little child that no matter where you're from, your dreams are valid.</i></b></font><br><br>
  	</div> 
  
  	<div> 
  			<div id="div1" class="fa"></div>
			<div id="text" align="left">
  			<font face="Berlin Sans FB Demi" color="black" size="4" align="center"><b>The World Awaits... </b></font><br><br>
  			</div> 
 	</div>
 
  	<div id="timer" align="center">
        	<div id="clockdiv" align="center">
            	<div align="center">
                	<span class="days"></span>
                	<div class="smalltext">Days</div>
            	</div>
            	<div align="center">
                	<span class="hours"></span>
                	<div class="smalltext">Hours</div>
            	</div>
            	<div align="center">
                	<span class="minutes"></span>
                	<div class="smalltext">Minutes</div>
            	</div>
            	<div align="center">
                	<span class="seconds"></span>
                	<div class="smalltext">Seconds</div>
            	</div>
        	</div>
    </div>
	<br><br><br><br><br>
	
	
	<div id="footer" style="background-color:#C0C0C0" align="center"> <br> <b> Follow us on <b> <br> <br> 
		<a href="#"> <img src="images/fb.png" height="30" width="30"> </img> </a> 
		<a href="#"> <img src="images/Instagram.png" height="30" width="30"> </img>  </a> 
		<a href="#"> <img src="images/twitter.png" height="30" width="30"> </img>  </a> <br> <br> </div> 
		<div style="display:table" align="center">
    			<div style="display: inline-block;width:400px;height:30px;background-color:#C0C0C0;" align="center"><a href="TermsOfUse.jsp"> Term of Use</a> </div>
    			<div style="display: inline-block;width:355px;height:30px;background-color:#C0C0C0;" align="right">Contact Us : </div>
   				<div style="display: inline-block;width:360px;height:30px;background-color:#C0C0C0;" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;lttsNationalAwards@gmail.com</div>
    			<div style="display: inline-block;width:400px;height:30px;background-color:#C0C0C0;" align="center"><a href="PrivacyPolicy.jsp">Privacy Policy</a></div>
		</div>
	<div style="background-color:#666666" align="center"> <br><font face="Calibri" > Copyright 2008 by Refrence data , Updated on 20th Sep 2018<br> </div>
	
	
<script>
	var slideIndex = 1;
	showDivs(slideIndex);
	function plusDivs(n) {
 							 showDivs(slideIndex += n);
						}
	function showDivs(n) {
  							var i;
  							var x = document.getElementsByClassName("mySlides");
  							if (n > x.length) {slideIndex = 1}    
  							if (n < 1) {slideIndex = x.length}
  							for (i = 0; i < x.length; i++) 
  							{
     							x[i].style.display = "none";  
  							}
  							x[slideIndex-1].style.display = "block";  
						}
	function hourglass() {
	  						var a;
	  						a = document.getElementById("div1");
	  						a.innerHTML = "&#xf251;";
	  						setTimeout(function () {
	      					a.innerHTML = "&#xf252;";
	    					}, 1000);
	  						setTimeout(function () {
	      					a.innerHTML = "&#xf253;";
	    					}, 2000);
						}
						hourglass();
						setInterval(hourglass, 3000);
	 function getTimeRemaining(endtime) {
		  					var t = Date.parse(endtime) - Date.parse(new Date());
		  					var seconds = Math.floor((t / 1000) % 60);
		  					var minutes = Math.floor((t / 1000 / 60) % 60);
		 	 				var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
		  					var days = Math.floor(t / (1000 * 60 * 60 * 24));
		  					return {
		    							'total': t,
		    							'days': days,
		    							'hours': hours,
		    							'minutes': minutes,
		    							'seconds': seconds
		  							};
						}
	function initializeClock(id, endtime) {
		  					var clock = document.getElementById(id);
		  					var daysSpan = clock.querySelector('.days');
		  					var hoursSpan = clock.querySelector('.hours');
		  					var minutesSpan = clock.querySelector('.minutes');
		  					var secondsSpan = clock.querySelector('.seconds');
		 					function updateClock() {
		    											var t = getTimeRemaining(endtime);
		    											daysSpan.innerHTML = t.days;
		    											hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
		    											minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
		    											secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
		    											if (t.total <= 0) 
		    											{
		      												clearInterval(timeinterval);
		    											}
		  											}
	  						updateClock();
	  						var timeinterval = setInterval(updateClock, 1000);
						}
						 var deadline = new Date(2018, 8, 13, 18, 00, 00);
						initializeClock('clockdiv', deadline);

</script>
</body>
</html>
   